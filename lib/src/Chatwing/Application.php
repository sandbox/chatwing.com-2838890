<?php
/**
 * @author chatwing <dev@chatwing.com>
 * @package Chatwing\SDK
 */

namespace Chatwing;

class Application
{
    /**
     * @var Container
     */
    protected static $container = null;

    /**
     * @param Container $c
     */
    public static function registerContainerInstance(Container $c)
    {
        static::$container = $c;
    }

    /**
     * @return Container
     */
    public static function getContainerInstance()
    {
        return static::$container;
    }

    public static function __callStatic($name, $arguments)
    {
        if (is_null(static::$container)) {
            throw new \Exception("Container has not been set!");
        }

        if (method_exists(static::$container, $name)) {
            return call_user_func_array(array(static::$container, $name), $arguments);
        } else {
            throw new \InvalidArgumentException("Method {$name} does not exist !!");
        }
    }
}
