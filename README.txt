CHATWING MODULE 7.x
Chatwing Live Group Chat - HTML5 + Chat Apps
=================

=============== SUMMARY ===============
Chatwing offers an unlimited live website/blog chat experience.This chat widget specializes in delivering real-time communication at any given time.

ChatWing is the ultimate free live chat service, instantly chat from any device. ChatWing is 100% cloud based live chat, talk on websites, via direct links like "chatwing.com/Mygroup" and inside this app. Instantly create new rooms, invite users, put any chat room directly on your website or blog and connect to the app. Also use ChatWing as a direct live chat messenger with friends, contacts or to meet new people.

- Create unlimited chat rooms with unlimited users.
- Choose to login with numerous social accounts including, Google, Facebook, Instagram, Twitter, Tumblr, Yahoo, Vkontacte, ChatWing and more.
- Use the lobby feature to join any existing room.
- Search for specific rooms you usually chat in on websites and blogs, take that room on the go once bookmarked.
- Bookmark unlimited chats and have instant access anytime.
- Many customization controls allow the ability to edit your text, fonts, insert video and images direct and much more.
- Connect your room to the Chatwing Android and IOS (ipad/ipad) apps.
- Receive alerts and notifications of new messages via chatwing mobile apps.
- Embed Chatwing into any webpage both Javascript and Iframe HTML code are available for each room.

=============== REQUIREMENTS ===============
Drupal 7.x

=============== INSTALLATION ===============
1. Copy the entire chatwing directory the Drupal sites/all/modules directory.
2. Login as an administrator. Enable the Chatwing module in the "Administer" -> "Modules".
3. Edit the settings under "Chatwing" -> "Configuration" -> "Chatwing settings".

4. Login your chatwing's account in chatwing.com, get access token and enter "Access token" textbox.
5. Enter "Save configuration".
6. You can see this at link: https://www.youtube.com/watch?v=hdmaSU7dZy0&t=7s.

=============== SUPPORT ===============
- You can create chatwing contents.
1. "Administer" -> "Content" -> "Add content" -> "Chatwing".
2. In "Create Chatwing", you will enter Chatwing Title, Body, Choose a chatbox in Chatbox combobox, height and width of chatbox.
3. Finally, I enter "Save".

- You can create chatwing block.
1. "Administer" -> "Blocks" -> Enabled Chatwing Block and choose a position for chatwing block. 
2. After you configure the chatwing block.
3. In "Chatwing Block" configuration, you enter: Block title, Select a chatbox, Width and Heigth chatbox.
4. Finally, I enter "Save block".

=============== FREQUENTLY ASKED QUESTIONS ===============

- The plugin requires me to insert Access token. Where can I get it ?.

Yes, you should have an access token in order to use this plugin. Please go to user control panel on ChatWing website to get that access token.