<?php

define("DEFAULT_WIDTH_CHATBOX", 200);
define("DEFAULT_HEIGHT_CHATBOX", 300);
define('CHATWING_MODULE_PATH', dirname(__FILE__));
define('CHATWING_LIB_PATH', 'lib/src/Chatwing');
if(!defined('DS')) { 
	define('DS', DIRECTORY_SEPARATOR);
}
define('CW_DEBUG', true);
define('CW_USE_STAGING', true);
define('CW_CLIENT_ID', 'drupal');
define('CHATWING_TEXTDOMAIN', 'chatwing');
