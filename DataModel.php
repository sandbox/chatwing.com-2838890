<?php 
namespace Chatwing;

/**
 * @package Chatwing
 * @author chatwing
 * Description: connect database and get data
 */

require_once dirname(__FILE__) . '/defines.php';
$keyFilePath = CHATWING_MODULE_PATH . DS . 'key.php';
if (file_exists($keyFilePath)) {
    include_once $keyFilePath;
}
require_once CHATWING_MODULE_PATH . DS . 'EncryptionHelper.php';
use EncryptionHelper as EncryptionHelper;

require_once CHATWING_MODULE_PATH . DS . 'APIContainer.php';
use Chatwing\APIContainer as APIContainer;

class DataModel {

  protected $token = "";
  protected $width = DEFAULT_WIDTH_CHATBOX;
  protected $height = DEFAULT_HEIGHT_CHATBOX;
  protected $chatbox = "";
  protected $baseUrl = null;

  // API information
  const API_VERSION = 2;
  const CLIENT_ID = 'drupal';

  function __construct() {
    
  }

  /**
  * save access token, width, height at chatwing_config table
  */
 public function save_chatwing_config($token = '', $width = DEFAULT_WIDTH_CHATBOX, $height = DEFAULT_HEIGHT_CHATBOX) {
    try {
      $token = base64_encode(EncryptionHelper::encrypt($token));
      db_insert('chatwing_config')
        ->fields(array(
            'cw_id' => '1',
            'cw_token' => $token,
            'cw_width' => $width,
            'cw_height' => $height,
            'cw_chatbox' => '',
      ))
    ->execute();

    } catch (Exception $e) {
      print $e->getMessage();
      return;
    }
  }

  /**
  * update access token, width, height at chatwing_config table
  */
  public function update_token_chatwing_config($token = '', $width = DEFAULT_WIDTH_CHATBOX, $height = DEFAULT_HEIGHT_CHATBOX) {
    try {
      $token = base64_encode(EncryptionHelper::encrypt($token));
      db_update('chatwing_config') // Table name no longer needs {}
        ->fields(array(
            'cw_token' => $token,
            'cw_width' => $width,
            'cw_height' => $height,
      ))
      ->condition('cw_id', 1)
      ->execute();

    } catch (Exception $e) {
      print $e->getMessage();
      return;
    }
  }

  /**
  * update access id chatbox, width, height at chatwing_config table
  */
  public function update_chatbox_chatwing_config($chatbox = '', $width = DEFAULT_WIDTH_CHATBOX, $height = DEFAULT_HEIGHT_CHATBOX) {
    try {
      db_update('chatwing_config') // Table name no longer needs {}
        ->fields(array(
            'cw_chatbox' => $chatbox,
            'cw_width' => $width,
            'cw_height' => $height,
      ))
      ->condition('cw_id', 1)
      ->execute();

    } catch (Exception $e) {
      print $e->getMessage();
      return;
    }
  }

  /**
  * get access token, width, height at chatwing_config table
  */
  public function get_chatwing_config($id = 1) {
    try {
      $result = db_query('SELECT cw_token, cw_width, cw_height, cw_chatbox FROM {chatwing_config} WHERE cw_id =' . $id)
                ->fetchAll();
      if (!empty($result)) {
        $result = $result[0];
      }
      return $result;
    } catch (Exception $e) {
      print $e->getMessage();
      return; 
    }
  }

  /**
  * check token exitsted
  */
  public function hasAccessToken() {
    return (bool) DataModel::getToken();
  }  

  /**
  * get token if token existed
  */
  public function getToken() {
    // check access token exists
    $cw_record = DataModel::get_chatwing_config();

    // if exists record, return API's token
    if (!empty($cw_record)) {
      $this->token =  EncryptionHelper::decrypt(base64_decode($cw_record->cw_token));
    }
    return $this->token;
  }

  /**
  * get width of chatbox
  */
  public function getWidth() {
    // check access token exists
    $cw_record = DataModel::get_chatwing_config();

    // if exists record, return API's token
    if (!empty($cw_record)) {
      $this->width = $cw_record->cw_width;
    }
    return $this->width;
  }

  /**
  * get height of chatbox
  */
  public function getHeight() {
    // check access token exists
    $cw_record = DataModel::get_chatwing_config();

    // if exists record, return API's token
    if (!empty($cw_record)) {
      $this->height = $cw_record->cw_height;
    }
    return $this->height;
  }

  /**
  * get height of chatbox
  */
  public function getChatbox() {
    // check access token exists
    $cw_record = DataModel::get_chatwing_config();

    // if exists record, return API's token
    if (!empty($cw_record)) {
      $this->chatbox = $cw_record->cw_chatbox;
    }
    return $this->chatbox;
  }

  /**
  * get Id of chatbox
  */
  public function getIdChatbox($keyChatbox) {
    // get chatbox list
     $boxes = DataModel::getBoxList();
     if(empty($boxes)) {
      return array();
    }

    foreach($boxes as $box) {
      if ($box['key'] == $keyChatbox) {
        return $box['id'];
      }
    }
    return '';
  }

  /**
  * get chatbox list
  */
  public function getBoxList() {
    $data = array();
    try { 
        $accessToken =  DataModel::getToken();
        $apiContainer = new APIContainer();
        $cwContainer = $apiContainer->getContainer();
        $cwContainer['api']->setAccessToken($accessToken);
        $response = $cwContainer['api']->call('user/chatbox/list');
    } catch (\Exception $e) {
      print $e->getMessage();
      return; 
    }
    if (!isset($response['data'])) {
      if (!empty($accessToken)) {
         drupal_set_message("There's no chatbox currently, or invalid token added", 'warning');
      }
      return array();
    }

    return $response['data'] ;
  }

  /**
  * get data for combobox
  */
  public function getChatListForCombobox() {
    $boxes = DataModel::getBoxList();
    if(empty($boxes)) {
      return array();
    }

    foreach($boxes as $box) {
        $data[$box['key']] = $box['name'];
    } 

    return $data;
  }

  /**
  * get Iframe string
  */
  function getIframe($key = '', $width = DEFAULT_WIDTH_CHATBOX, $height = DEFAULT_HEIGHT_CHATBOX) {
    $apiContainer = new APIContainer();
    $cwContainer = $apiContainer->getContainer();
    $box = $cwContainer['cb'];
    $url = "";
    if (empty($key)) {
      return "";
    }
    if (empty($width)) {
      $width = DEFAULT_WIDTH_CHATBOX;
    }
    if (empty($height)) {
      $height = DEFAULT_HEIGHT_CHATBOX;
    }

    // get chatbox url in chatwing 
    $idChatbox = DataModel::getIdChatbox($key);
    if (empty($idChatbox)) {
      return;
    }
    $url = DataModel::getBaseUrl($idChatbox);
    $box->setKey($key);
    return  "<iframe src='". $url."' frameborder='0' width='".$width."' height='".$height."'></iframe>";
  }

  /**
  * get user information to register chatwing
  */
  public function getBaseUrl($idChatbox) {  
    $params = array('id' => $idChatbox, 'plugin' => 'drupal');

    // get information of user login
    global $user;
    if ($user->uid) {
      $imgpath = file_load($user->picture); 
      $avatarCustomer = 'http://chatwing.com/images/no-avatar.gif';
      if ($imgpath) {
         $avatarCustomer = file_create_url($imgpath->uri);
      }
      $chatboxData =  array();
      $params["dr_user"]['ID'] = $user->uid;
      $params["dr_user"]['avatar']  = $avatarCustomer;
      $params["dr_user"]['display_name']  = $user->name;
      $params["dr_user"]['user_email']  = $user->mail;
      $params["dr_user"]['user_pass']  = $user->pass;
      $params["dr_user"]['user_login']  = $user->name;
      $params["dr_user"]['user_nicename']  = $user->name;
    }

    if (is_null($this->baseUrl)) {
      // get chatbox data
      $accessToken =  DataModel::getToken();
      $apiContainer = new APIContainer();
      $cwContainer = $apiContainer->getContainer();
      $cwContainer['api']->setAccessToken($accessToken);
      $response = $cwContainer['api']->call('chatbox/read',$params);
      if(empty($response)) {
          throw new ChatwingException(__("Invalid chatbox ID", CHATWING_TEXTDOMAIN));
      } else {
          $chatboxData = $response->get('data');
          $this->baseUrl = $chatboxData['urls']['full'];
      }
        
    }
    $this->baseUrl.="?";
    if (!empty($chatboxData)) {
      if (!empty($chatboxData["json"]["drupalSynchronize"]) &&
          ($chatboxData["json"]["drupalSynchronize"] ==  true || $chatboxData["json"]["drupalSynchronize"] == "true")) {
        $this->baseUrl.="plugin=drupal&client_id=drupal&hide_login=true";
        if (!empty($chatboxData["chatuser"]) && !empty($chatboxData["chatuser"]["access_token"])) {
          $this->baseUrl.="&access_token=".$chatboxData["chatuser"]["access_token"];
        } else {
          $this->baseUrl.="&logout=true";
        }
      }      
    }
    return  $this->baseUrl;
  }
}
