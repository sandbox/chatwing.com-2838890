<?php

/**
 * @file
 * create chatwing block, config and get data for this block
 * @package Chatwing
 * @author chatwing
 */

require_once dirname(__FILE__) . '/defines.php';
require_once CHATWING_MODULE_PATH . DS . 'DataModel.php';
use Chatwing\DataModel as DataModel;

/**
 * Implements hook_block_info().
 */
function chatwing_block_info() {
  $blocks = array();
  $blocks['chatwing_block'] = array(
    'info' => t('Chatwing Block'),
  );
  return $blocks;
}

/**
 * Implements hook_block_configure().
 */
function chatwing_block_configure($delta = '') {
  $form = array();
  
  switch ($delta) {
    case 'chatwing_block':
      $dataModel = new DataModel();
      $width = $dataModel->getWidth();
      $height = $dataModel->getHeight();
      $boxes = $dataModel->getChatListForCombobox();
      $chatbox = $dataModel->getChatbox();

      $form['chatbox'] = array(
        '#title' => t('Select a chatbox'),
        '#type' => 'select',
        '#class' => 'pure-control-group',
        '#options' => $boxes,
        '#default_value' => $chatbox,
      );
      $form['width'] = array(
        '#title' => t('Width chatbox'),
        '#type' => 'textfield',
        '#class' => 'pure-control-group',
        '#name' => 'width',
        '#id' => 'width',
        '#default_value' => $width,
      );
      $form['height'] = array(
        '#title' => t('Heigth chatbox'),
        '#type' => 'textfield',
        '#class' => 'pure-control-group',
        '#name' => 'height',
        '#id' => 'height',
        '#default_value' => $height,
      );
      $form['#validate'][] = 'chatwing_block_config_form_validate';
      $form['#submit'][] = 'chatwing_block_config_form_submit';
      break;
  }
  return $form;
}
/**
 * Implements hook_config_form_validate().
 */
function chatwing_block_config_form_validate() {
  // Width is required and number type.
  if (!empty($width) && !is_numeric($width)) {
    form_set_error('width', 'Width of chatbox is number type (example: 100, 200, 300 ...)');
  }
  // Height is required and number type.
  if (!empty($height) && !is_numeric($height)) {
    form_set_error('height', 'Height of chatbox is number type (example: 100, 200, 300 ...)');
  }
}

/**
 * Implements hook_save().
 */
function chatwing_block_save($delta = '', $edit = array()) {
  switch ($delta) {
    case 'chatwing_block':
      $dataModel = new DataModel();
      $chatbox = $edit['chatbox'];
      $width = $edit['width'];
      $height = $edit['height'];

      $dataModel->update_chatbox_chatwing_config($chatbox, $width, $height);
      break;
  }
}

/**
 * Implements hook_view().
 */
function chatwing_block_view($delta = '') {
  $block = array();
  switch ($delta) {
    case 'chatwing_block':
      $dataModel = new DataModel();
      $width = $dataModel->getWidth();
      $height = $dataModel->getHeight();
      $chatbox = $dataModel->getChatbox();

      $block['subject'] = t('Chatwing');
      $block['content'] = $dataModel->getIframe($chatbox, $width, $height);
      
      break;
  }
  return $block;
}