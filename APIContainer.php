<?php
namespace Chatwing;

/**
 * @package Chatwing
 * @author chatwing
 * Description: init Container object to connect API
 */

require_once dirname(__FILE__) . '/defines.php';
require_once CHATWING_MODULE_PATH . DS . CHATWING_LIB_PATH . DS . 'autoloader.php';
require_once CHATWING_MODULE_PATH . DS . CHATWING_LIB_PATH . DS . 'start.php';
require_once CHATWING_MODULE_PATH . DS . CHATWING_LIB_PATH . DS . 'Application.php';

class APIContainer {

	public function getContainer() {
	    $cwContainer = new \Chatwing\Container();

	    $cwContainer['api'] = function (\Chatwing\Container $c) {
	      $api = new \Chatwing\Api(CW_CLIENT_ID);
	      $api->setEnv(CW_DEBUG && defined('CW_USE_STAGING') && CW_USE_STAGING ? CW_ENV_DEVELOPMENT : CW_ENV_PRODUCTION);

	      if (isset($c['cw_token'])) {
	        $api->setAccessToken($c['cw_token']);
	      }

	      return $api;
	    };

	    $cwContainer->factory('cb', function (\Chatwing\Container $c) {
	      return new \Chatwing\Chatbox($c['api']);
	    });

	    $cwContainer = $cwContainer->factory('cb_session', function (\Chatwing\Container $c) {
	      return new \Chatwing\Encryption\Session($c['cb_secret']);
	    });

	    return $cwContainer;
  	}
}