<?php
/**
 * @package Chatwing
 * @author chatwing
 * Description: create chatwing content type
 */

require_once dirname(__FILE__) . '/defines.php';
require_once CHATWING_MODULE_PATH . DS . 'DataModel.php';
use Chatwing\DataModel as DataModel;

/**
 * Implements hook_node_info()
 */
function chatwing_node_info() {
    return array(
        'chatwing' => array(
            'name' => t('Chatwing'),
            'base' => 'chatwing',
            'description' => t('You can define new Chatbox here'),
            'has_title' => false,
         )
    );
}

/**
 * Implement hook_form()
 */
function chatwing_form($node, $form_state) {
  $dataModel = new DataModel();
  $boxes = $dataModel->getChatListForCombobox();
  // Initiate the cw_chatbox field
  $field_cw_chatbox =  array(
      'field_name' => 'cw_chatbox',
      'type' => 'list_text',
      'cardinality' => 1,
      'settings' => array(
        'allowed_values' => $boxes,
      )
  );
  // Initiate the cw_chatbox instance.
  $instance_cw_chatbox = array( 'field_name' => 'cw_chatbox',
    'entity_type' => 'node',
    'bundle' => 'chatwing',
    'label' => 'Chatbox',
    'description' => 'Select a chatbox.',
    'required' => false,
      'widget' => array(
        'type' => 'options_select',
    ),
  );

  // Initiate the cw_width field.
  $field_cw_width = array(
    'field_name' => 'cw_width',
    'type' => 'number_integer',
  );
  // Initiate the cw_width instance.
  $instance_cw_width = array( 'field_name' => 'cw_width',
    'entity_type' => 'node',
    'bundle' => 'chatwing',
    'label' => 'Width',
    'description' => 'The width of chatbox.',
    'widget' => array(
        'type' => 'textfield',
    ), 
  );

  // Initiate cw_height field
  $field_cw_height = array(
      'field_name' => 'cw_height',
      'type' => 'number_integer',
  );
  // Initiate the cw_height instance.
  $instance_cw_height = array( 'field_name' => 'cw_height',
    'entity_type' => 'node',
    'bundle' => 'chatwing',
    'label' => 'Height',
    'description' => 'The height of chatbox.',
    'widget' => array(
        'type' => 'textfield',
    ), 
  );

  try {
    // Create cw_chatbox field and cw_chatbox instance
    if (!field_info_field('cw_chatbox')) {
      field_create_field($field_cw_chatbox);  
      field_create_instance($instance_cw_chatbox);
    } else if (!empty($boxes)) {
      $field_cw_chatbox = field_info_field('cw_chatbox');
      $field_cw_chatbox['settings']['allowed_values'] = $boxes;
      field_update_field($field_cw_chatbox);
    }

     // Create cw_width field and cw_width instance
    if (!field_info_field('cw_width')) {
      field_create_field($field_cw_width);
      field_create_instance($instance_cw_width);
    }

    // Create cw_height field and cw_height instance
    if (!field_info_field('cw_height')) {
      field_create_field($field_cw_height);
      field_create_instance($instance_cw_height);
    } 
  } catch (Exception $e) {
    // if cw_chatbox existed, we will delete cw_chatbox field
    if (field_info_field('cw_chatbox')) {
      field_delete_field('cw_chatbox');
    }
    // if cw_width existed, we will delete existed field
    if (field_info_field('cw_width')) {
       field_delete_field('cw_width');
    }
    // if cw_height existed, we will delete cw_height field
    if (field_info_field('cw_height')) {
      field_delete_field('cw_height');
    }

    // try to create cw_chatbox field, cw_width field, cw_height field again
    field_create_field($field_cw_chatbox);  
    field_create_instance($instance_cw_chatbox);
    field_create_field($field_cw_width);
    field_create_instance($instance_cw_width);
    field_create_field($field_cw_height);
    field_create_instance($instance_cw_height);

  } finally {
    $form['title'] = array(
      '#type' => 'textfield',
      '#title' => 'Chatwing Title',
      '#required' => FALSE,
      '#default_value' => $node->title,
      '#weight' => -5
    );

    return $form;
  }
}
function chatwing_node_view($node, $view_mode, $langcode = NULL) {
  if (!empty($node) && ($node ->type == 'chatwing')) {
    $dataModel = new DataModel();
    $key =  '';
    $width = '';
    $height = '';
    if (isset($node->content['cw_chatbox']['#items'][0]['value'])) {
      $key = $node->content['cw_chatbox']['#items'][0]['value'];
    }
    if (isset($node->content['cw_width']['#items'][0]['value'])) {
        $width = $node->content['cw_width']['#items'][0]['value'];
    }
    if (isset($node->content['cw_height']['#items'][0]['value'])) {
      $height = $node->content['cw_height']['#items'][0]['value'];
    }
    $iframe = $dataModel->getIframe($key, $width, $height);
    $node->content = array();
    $node->content['chatwing'] = array(
      '#markup' => $iframe,
      '#weight' => 10
    );
  }
  return $node;
}